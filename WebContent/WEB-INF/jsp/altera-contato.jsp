<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="senai"%>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
	<meta charset="UTF-8">
	<title>Agenda - Cadastro de Contato</title>
	
	<link rel="stylesheet" href="css/jquery-ui.min.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	</head>
	<body>
		<c:import url="cabecalho.jsp" />
		
			<form action="mvc?logica=EditaContatoLogica&id=${requestScope.id }" method="post">
				
			    	Nome<br>
			    	<input type="text" name="nome" value="${requestScope.nome }" placeholder="Nome">
			  		<br><br>
			    	E-mail<br>
			    	<input type="text" name="email" value="${requestScope.email }" placeholder="E-mail">
			  		<br><br>
			    	Endere�o<br>
			    	<input type="text" name="endereco" value="${requestScope.endereco }" placeholder="Endere�o">
			  		<br><br>
				    Data de nascimento<br>
				    <senai:campoData id="dataNascimento" value="${requestScope.dataNascimento }"/>
				    <br><br>
					<input type="submit" name="Salvar" value="Salvar">
			</form>
		
		<c:import url="rodape.jsp" />
	</body>
</html>
package br.senai.sp.informatica.agenda.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.agenda.dao.ContatoDao;
import br.senai.sp.informatica.agenda.model.Contato;

public class RemoveContatoLogica implements Logica{

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		// Obt�m o id por meio de parametro
		// localhost:8080/agenda-web/mvc?logica=RemoveContatoLogica&id=${contato.id}
		
		long id = Long.parseLong(req.getParameter("id"));
		
		Contato contato = new Contato();
		contato.setId(id);
		
		ContatoDao dao = new ContatoDao();
		dao.excluir(contato);
		
		System.out.println("Excluindo o contato...");
		
		// apos excluir redireciona para listar
		return "mvc?logica=ListaContatosLogica";
	}
	
	
}

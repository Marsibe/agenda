package br.senai.sp.informatica.agenda.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.senai.sp.informatica.agenda.model.Contato;

public class ContatoDao {

	// Atributo
	private Connection connection;

	// Construtor
	public ContatoDao() {
		// Estabelece uma conex�o com o banco de dados
		connection = new ConnectionFactory().getConnection();
	}

	// Salva
	public void salva(Contato contato) {
		String sql = null;
		
		if (contato.getId() != null) {
			sql = "UPDATE contato SET nome = ?, email = ?, endereco = ?, "
					+ "dataNascimento = ? WHERE id = ?";
		} else {
			// Cria um comando sql
			sql = "INSERT INTO contato " + "(nome, email, endereco, dataNascimento) " 
			+ "VALUES (?, ?, ?, ?)";
		}
		

		try {
			// Cria um preparedstatement
			PreparedStatement stmt = connection.prepareStatement(sql);

			// Cria os par�metros do PreparedStatement
			stmt.setString(1, contato.getNome());
			stmt.setString(2, contato.getEmail());
			stmt.setString(3, contato.getEndereco());

			stmt.setDate(4, new Date(contato.getDataNascimento().getTimeInMillis()));
			if (contato.getId() != null) {
				stmt.setLong(5, contato.getId());
			}
			
			// executa o insert
			stmt.execute();

			// libera o recurso statement
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	// getlista
	public List<Contato> getLista() {
		try {
			// Cria um ArrayList
			List<Contato> contatos = new ArrayList<>();

			// Cria um PreparedStatement
			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM contato");

			// executa o statement e guarda o resultado em um resultset
			ResultSet rs = stmt.executeQuery();

			// enquanto houver dados no resultset
			while (rs.next()) {
				// cria um contato com os dados do resultset
				Contato contato = new Contato();
				contato.setId(rs.getLong("id"));
				contato.setNome(rs.getString("nome"));
				contato.setEmail(rs.getString("email"));
				contato.setEndereco(rs.getString("endereco"));

				// Obtem uma instancia de calendar
				Calendar data = Calendar.getInstance();

				data.setTime(rs.getDate("dataNascimento"));

				contato.setDataNascimento(data);

				contatos.add(contato);
			}

			rs.close();

			stmt.close();

			return contatos;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void excluir(Contato contato) {
		try {
			PreparedStatement stmt = connection.prepareStatement("DELETE FROM contato WHERE id = ?");
			stmt.setLong(1, contato.getId());
			stmt.execute();
			stmt.close();
		}catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
